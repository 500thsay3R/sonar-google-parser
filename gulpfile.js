/* eslint strict: 0 */
'use strict';

const gulp = require('gulp');
const nodemon = require('gulp-nodemon');
const eslint = require('gulp-eslint');
const notify = require('gulp-notify');
const logger = require('better-console');
const mocha = require('gulp-mocha');

function lint() {
  gulp.src(['./helpers/**/*.js', 'app.js'])
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(notify({ title: 'ESLint', message: 'ESLint passed. Let it fly!' }));
}

function test() {
  gulp.src(['./test/*.js'])
    .pipe(mocha({ ui: 'tdd', timeout: 60000 }))
    .pipe({ title: 'Unit Tests', message: 'All unit tests passed!', onLast: true })
    .on('error', () => notify.onError('Error <%= error.message %>'));
}

gulp.task('lint', lint);
gulp.task('test', test);

gulp.task('nodemon', () => {
  const nodemonConfig = {
    ignore: ['.git', 'node_modules/**/node_modules'],
    watch: ['*.js', 'helpers'],
    verbose: true,
    execMap: {
      js: '~/.nvm/versions/node/v5.10.1/bin/node --harmony_destructuring ' +
      '--harmony_default_parameters',
    },
    script: 'app.js',
    ext: 'js json',
    env: { NODE_ENV: 'development' },
    tasks: ['default'],
  };

  nodemon(nodemonConfig)
    .on('start', () => logger.clear())
    .on('restart', () => {
      logger.clear();
      lint();
      test();
    });
});

gulp.task('default', ['lint', 'nodemon']);
