/* eslint-env: node, es6 */
/* eslint strict: 0 */
/* eslint no-console: 0 */
/* eslint arrow-body-style: 0 */
/* eslint no-param-reassign: 0 */
/* eslint no-underscore-dangle: 0 */
/* eslint no-return-assign: 0 */
'use strict';

const url = require('url');
const _ = require('lodash');
const cheerio = require('cheerio');
const request = require('request');
const querystring = require('querystring');
const winston = require('winston');

// set up logging preferences
const logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({
      prettyPrint: true,
      level: process.env.NODE_ENV,
    }),
  ],
});

const countries = require('./lib/country-settings.json');

/** Class Representing Google Search request. */
class GoogleSearch {
  /**
   * Create a search request.
   * @param {Object}    params                - The search parameters.
   * @param {!String}   params.query          - The search key.
   * @param {?String}   [params.country=null] - The country to get results for.
   * @param {?String}   [params.period=y]     - The search timeframe.
   * @param {?String}   [params.sort=rel]     - The sort order.
   * @param {String[]}  [params.inc=[]]       - The words every web-page must contain.
   * @param {String[]}  [params.exc=[]]       - The words web-pages should not contain.
   * @param {Number}    [params.limit=500]    - Defines # of resultant web-pages to retrieve.
   *
   * @property {String} NEXT_TEXT - defines Next-button text.
   */
  constructor(params) {
    const { locale = null, period = 'y', exc = [], inc = [], limit = 500, target = null } = params;

    this.query = params.query;
    this.country = locale;
    this.period = period;
    this.sort = (period === 'y')
      ? 'rel'
      : 'date';
    this.exclusions = exc;
    this.inclusions = inc;
    this.pages = Math.floor(limit / 100);
    this.results = new Set();
    this.target = target;
  }

  /**
   * Form up body of URL request.
   * @param {Number} [since=0] - paging parameter.
   * @returns {String} - the well-formed Google Search request URL
   *
   * @property tld  - Google TLD.
   * @private
   */
  _formRequest(since = 0) {
    const requestOptions = {
      q: this.query,    // the search key
      hl: 'en',         // sets up language of Google Interface
      ie: 'utf-8',      // incoming encoding
      oe: 'utf-8',      // outgoing encoding
      num: 100,         // results per page
      start: since,     // paging
      tbs: `qdr:${this.period},sbd:${this.sort === 'rel' ? 0 : 1}`, // time and sort settings
    };

    // set global Google as default SE
    let tld = '.com';

    // make Google return country-specific results by using cr parameter and local version of Google
    if (this.country && countries[this.country]) {
      requestOptions.cr = `country${this.country.toLocaleUpperCase()}`;
      tld = countries[this.country].tld;
    }

    // enhance search key by adding plus & minus-words and target-site
    if (this.exclusions.length) {
      requestOptions.q += ` -${this.exclusions.join(' -')}`;
    }
    if (this.inclusions.length) {
      requestOptions.q += ` +${this.inclusions.join(' +')}`;
    }
    if (this.target) {
      requestOptions.q += ` site:${this.target}`;
    }

    return url.format({
      protocol: 'https:',
      host: `www.google${tld}`,
      pathname: '/search',
      search: querystring.stringify(requestOptions),
    });
  }

  /**
   * Retrieves Google results.
   * @param {String} urlRequest   -  The well-formed URL to connect to.
   * @returns {Promise.<String>}  - The page contents.
   * @private
   */
  _performRequest(urlRequest) {
    return new Promise((resolve, reject) => {
      const requestOptions = {
        url: url.format(urlRequest),
        timeout: 15000,
        rejectUnauthorized: false,  // prevents unauthorized connection from being dropped
      };

      request(requestOptions, (error, response) => {
        if (!error && response && response.statusCode === 200) {
          logger.debug(`@GOOGLE :: Successfully retrieved data from ${urlRequest}`);

          resolve(response.body.toString());
        } else {
          logger.warn(`@GOOGLE :: [${(response) ? response.statusCode : 'ERR'}] Failed to ` +
            `retrieve data from ${urlRequest} -> [${error || 'UNRECOGNIZED ERROR'}].`);

          // handle Google bans
          if (response && response.body.indexOf('traffic')) {
            logger.error('@GOOGLE :: Google ban confirmed. Requests are temporarily forbidden!');

            reject(new Error('Google ban'));
          }

          reject(new Error('Google connection failure'));
        }
      });
    });
  }

  /**
   * Parses data from each Google results page.
   *
   * @param {String} pageContents     -   HTML contents of Google results page.
   * @param {!Number} pageNumber      -   The number of page that is currently being processed.
   *
   * @returns {Promise.<Object[]>}    -   A bunch of resultant objectsEach of them includes:
   *          { url: {String}, title: {String}, description: {String}, index: {Number}}.
   * @private
   */
  _processPage(pageContents, pageNumber) {
    const $ = cheerio.load(pageContents);

    // remove extra whitespaces & newline characters
    function prettyPrint(text) {
      return text.replace(/\n/g, ' ').replace(/\s+/g, ' ');
    }

    // analyse each and every resultant link element
    function analyze(elem, pageNum, position) {
      return new Promise((resolve) => {
        // distinguish links and their textual description
        const text = $(elem).find('div.s'); // CSS selector that describes links' text description
        const link = $(elem).find('h3.r a'); // CSS selector that leads to the link itself

        const searchItem = {
          index: pageNum * 100 + position,
          url: querystring.parse($(link).attr('href'))['/url?q'], // omitting Google redirect stuff
          title: prettyPrint($(link).first().text()),
        };

        // process description
        $(text).find('div').remove();
        searchItem.description = prettyPrint($(text).text());

        // omit empty elements
        resolve(searchItem.url ? searchItem : null);
      });
    }

    // process all the result elements in parallel
    const resultElements = $('div.g');
    const tasks = _.map(resultElements, (elem, index) => analyze(elem, pageNumber, ++index));

    // check if the next page of results does indeed exist
    const moreToCome = ($('td.b a span').last().text() === 'Next');

    return Promise.all(tasks)
      .then(results => {
        return { results: _.compact(results), moreToCome };
      });
  }

  /**
   * Performs consecutive results extraction & processing
   * @param {!Number} currentPage   -   Current page of Google Search results
   * @param {!Number} since         -   Paging step.
   * @returns {Promise.<Object[]>}  -   Retrieved results.
   */
  find(currentPage = 0, since = 0) {
    return new Promise((resolve) => {
      logger.debug(`@GOOGLE:: Processing page #${currentPage + 1}.`);

      // perform extraction until the page limit is reached
      if (currentPage < this.pages) {
        this._performRequest(this._formRequest(since))
          .then(html => this._processPage(html, currentPage))
          .then(data => {
            logger.info(`@GOOGLE :: ${data.results.length} links parsed from p.${currentPage}. ` +
              `More is about to come: ${data.moreToCome}.`);

            _.map(data.results, dataElement => this.results.add(dataElement));

            // if there's no more pages to process, return results or
            // process the next page otherwise
            resolve(data.moreToCome ? this.find(++currentPage, since += 100) : [...this.results]);
          })
          .catch(error => {
            logger.warn(`@GOOGLE :: Links retrieval was interrupted -> ${error.message}.`);
            const leads = [...this.results];

            if (leads && !leads.length) {
              logger.warn(`@GOOGLE :: [${this.query} --${this.period}] -> **NO** leads to return.`);
              throw new Error('No leads retrieved');
            } else {
              resolve(leads);
            }
          });
      } else {
        const leads = [...this.results];

        if (leads && !leads.length) {
          logger.info(`@GOOGLE :: [${this.query} --${this.period}] -> **NO** leads retrieved`);
          throw new Error('No leads retrieved');
        } else {
          resolve(leads);
        }
      }
    });
  }
}

module.exports = GoogleSearch;
