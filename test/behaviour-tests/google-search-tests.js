/* eslint-env: mocha */
/* eslint strict: 0 */
/* eslint no-undef: 0 */
/* eslint no-underscore-dangle: 0 */
/* eslint arrow-body-style: 0 */
'use strict';

const url = require('url');
const _ = require('lodash');
const querystring = require('querystring');
const chai = require('chai');
chai.should();

chai.use(require('chai-things'));
chai.use(require('chai-as-promised'));

const GoogleSearch = require('../../app.js');
const countries = require('../../lib/country-settings.json');

suite('#_formRequestQuery()', () => {
  let basicQuery;
  let complexQuery;
  const basicInfo = { query: 'Saab' };
  const complexInfo = {
    query: 'Volvo',
    country: 'no',
    sort: 'date',
    period: 'm',
    exc: ['Geely', 'China'],
    inc: ['Polestar', 'S60'],
  };

  suiteSetup(() => {
    const basicReq = new GoogleSearch(basicInfo);
    basicQuery = basicReq._formRequest();

    const complexReq = new GoogleSearch(complexInfo);
    complexQuery = complexReq._formRequest();
  });

  suite('Test basic requests processing', () => {
    test('Should correctly autocomplete missing search parameters', () => {
      basicQuery.should.be.a('string').that.contains('qdr%3Ay%2Csbd%3A0');
    });
  });

  suite('Test complex requests processing', () => {
    test('Should include all the plus-words within `q` field', () => {
      complexQuery.should.contain(querystring.stringify(` +${complexInfo.inc.join(' +')}`));
    });

    test('Should include all the minus-words within `q` field', () => {
      complexQuery.should.contain(querystring.stringify(` -${complexInfo.exc.join(' -')}`));
    });
  });
});

suite.skip('#find()', () => {
  let indices;

  suiteSetup(() => {
    indices = _.keys(countries);
  });

  test('all responses should be arrays', () => {
    _.forEach(indices, index => {
      const request = {
        query: 'Volvo',
        country: index,
        sort: 'date',
        period: 'm',
        exc: ['Geely', 'China'],
        inc: ['Polestar', 'S60'],
        limit: 100,
      };

      const g = new GoogleSearch(request);
      g.find().should.eventually.all.contain.all.keys(['url', 'title', 'description', 'index']);
    });
  });
});
