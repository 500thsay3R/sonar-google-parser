/* eslint-env: node, es6, mocha */
/* eslint strict: 0 */
/* eslint no-undef: 0 */
/* eslint no-underscore-dangle: 0 */
/* eslint arrow-body-style: 0 */
'use strict';

const chai = require('chai');
chai.should();

chai.use(require('chai-things'));

const countries = require('../../lib/country-settings.json');

suite('Test countries\' data', () => {
  let keys;
  const tlds = [];
  suiteSetup(() => {
    keys = Object.keys(countries);

    for (const key of keys) {
      tlds.push(countries[key].tld);
    }
  });

  test('All country codes should be valid ISO-3166 Alpha 2 codes', () => {
    return keys.should.all.match(/[a-z]{2}/);
  });

  test('All TLDs should begin with "."', () => {
    return tlds.should.all.match(/.\D+/);
  });

  test('All TLDs should be valid', () => {
    for (const tld of tlds) {
      if (tld.length === 3) {
        tld.should.match(/.[a-z]{2}/);
      } else if (tld.length === 4) {
        tld.should.eql('.com');
      } else if (tld.length === 6) {
        tld.should.match(/.co.[a-z]{2}/);
      } else if (tld.length === 7) {
        tld.should.match(/.com.[a-z]{2}/);
      } else {
        throw new Error(`Invalid TLD -> ${tld}`);
      }
    }
  });
});
